const express = require('express');
const path = require('path');

class App {
    constructor(db) {
        this._db = db;
        this._app = express();
        this._app.use(express.json());
        this._app.use('/', express.static(path.resolve(__dirname, '../public')));

        this._app.post('/logIn', this.logIn);
        this._app.post('/userInfo', this.submitUser);

    }
    
    logIn = (req, res) => {
        const { user, password } = req.body;
        
        res.send(this._db.checkAdmin(user, password));
        res.end();
    };

    submitUser = (req, res) => {
        res.send(this._db.addUserToDb(req.body));

        res.end();
    };
    
    getApp = () => this._app;
}

module.exports = App;
